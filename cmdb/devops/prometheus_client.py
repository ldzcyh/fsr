# ~*~ coding: utf-8 ~*~
# auth: haochenxiao

import datetime
import redis
import json
from confs.Configs import *
from confs.Log import logger
#redis
redis_db = 2
redisPool = redis.ConnectionPool(host=redis_host,port=redis_port,db=redis_db,password=redis_password)
client = redis.Redis(connection_pool=redisPool)
flag = 'icbc'

def PrometheusClient(datasource,ip,action):
	# 发送状态到故障自愈系统处理

	nowtime=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	ms = {
		'datasource': datasource,
		'tm':nowtime,
		'IP': ip,
		'action': action
	}

	vfs = json.dumps(ms)
	client.lpush(flag, vfs)
	logger.info('发送%s %s 到redis'%(ip,action))






