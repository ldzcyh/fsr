from django.conf.urls import url, include
from django.views.generic import RedirectView
from syscmdb.views import *
from django.contrib import admin
handler404 = page_not_found
handler500 = page_error

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/dashboard/')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^resources/', include('resources.urls')),
    url(r'^products/', include('products.urls')),
	url(r'^devops/', include('devops.urls')),
    url(r'^alert/', include('alert.urls')),
    url(r'^dbmonitor/', include('dbmonitor.urls')),

]
