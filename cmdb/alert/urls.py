# _*_ coding: utf-8 _*_
__author__ = 'HaoGe'
from django.conf.urls import url, include

from alert import views
urlpatterns = [
    url(r'^alert_history/$', views.AlertHistoryView.as_view(), name='alert_history'),
    url(r'^api_alert/$', views.APIAlertHistoryView.as_view(), name='api_alert'),
    url(r'^alert_linkperson/$', views.LinkPersonView.as_view(), name='alert_linkperson'),
    url(r'^api_linkperson/$', views.APILinkPersonView.as_view(), name='api_linkperson'),
    url(r'^linkperson_edit/$', views.PersonEditView.as_view(), name='linkperson_edit'),
    url(r'^alert_linkperson_delete/$', views.LinkPersonDeleteView.as_view(), name='alert_linkperson_delete'),
    url(r'^alert_linkgroup/$', views.LinkGroupView.as_view(), name='alert_linkgroup'),
    url(r'^api_alert_linkgroup/$', views.APILinkGroupView.as_view(), name='api_alert_linkgroup'),
    url(r'^linkgroup_create/$', views.LinkGroupCreateView.as_view(), name='linkgroup_create'),
    url(r'^linkgroup_edit/$', views.LinkGroupEditView.as_view(), name='linkgroup_edit'),
    url(r'^alert_linkgroup_delete/$', views.LinkGroupDeleteView.as_view(), name='alert_linkgroup_delete'),
    url(r'^api_operation/$', views.APIOperation, name='api_operation'),
    url(r'^attentionconfig/$', views.AttenTionConfigView.as_view(), name='attentionconfig'),
]