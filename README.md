# 简介

本项目是基于python3  django 开发的集资产管理系统+监控系统+顺序多主机自愈系统为一体的运维管理故障自愈系统。

#功能：
  
* 资产管理,支持excel csv格式批量导入资产主机
* 定时任务
* 自带url和端口监控
* 通过prometheus+alertmanager的webhook配置fsr实现故障自愈
* 告警信息归类触发自定义动作达到自愈目的
* 告警和自愈历史查看
* 通过API触发自定义动作
* 客户端直接启动二进制监控脚本即可对远程主机cpu,内存,进程数,磁盘，网卡流量等进行图表监控，方便历史查看分析
* mysql 添加了 qps,tps,连接数,流量,IO,KEY BUFFER等监控,并增加了mysql慢查询功能
* oracle 表空间监控
* 自愈支持顺序多台执行自愈动作
* python版本 lepus

#加QQ群

* 群号:  817399355

* 开发不易,给个star吧
  
# 环境要求:
    系统:     Unix & Linux 平台安装 
    语言：    python 3.6.x 版本
    数据库:   mysql 5.6
              redis 无版本要求

# docker安装步骤：
1. **docker docker-compose安装(略)**
2. **进入fsr根目录后执行命令**。

        1> cp conf/docker_db.conf.demo conf/db.conf

        2> docker-compose up 等待环境自动安装

        3> sh install/init.sh 
    
        4> docker-compose stop && docker-compose up -d

        5> 以上步骤执行完毕,可以打开,该服务器ip:12000 端口进行访问. 账号:admin 密码: admin321666

# 二开安装步骤：

1. **python3 mysql redis 环境安装(略)**

        看下sshpass有没有安装,没有的执行 yum -y install sshpass，否则后面ssh 主机链接会报错。

        环境安装好后,数据库需要自己提前创建,数据库中执行建库命令,密码需要自己创建。 
        CREATE DATABASE IF NOT EXISTS fsr DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

2. **参考conf/db.conf.demo 配置自己的环境信息**。
     
            cp conf/db.conf.demo conf/db.conf,进入conf目录修改db.conf配置信息

            [redis]                    #连接redis配置
            host =                     #redis服务ip地址
            port =                     #redis服务端口号
            password =                 #redis密码
            db = 2                     #默认是2,可以修改
            
            [mysql_config]             #连接mysql配置
            db = fsr                   #数据库名称
            host = yourip              #数据库地址
            user = youruser            #连接数据库用户
            passwd = yourpassword      #连接数据库密码
            port = 3306                #连接数据库端口,默认3306
            charset = utf8             #默认编码
            timeout = 600              #超时时间
            
            [mail_celery]              #自愈系统发送邮件配置
            host = yourhost            #邮件服务器地址
            username = username        #邮箱账号
            password = password        #邮箱密码
            user = user                #昵称
            
            [CeleryConfig]                                                        #celery异步配置
            BROKER_URL = redis://:password@yourip:yourport/5                      #celery BROKER地址,数据库默认使用5,(这里配置redis的密码和ip)
            CELERY_RESULT_BACKEND = redis://:password@yourip:yourport/5           #celery RESULT_BACKEND地址,数据库默认使用5,(这里配置redis的密码和ip)
            CELERY_ACCEPT_CONTENT = application/json
            CELERY_TASK_SERIALIZER = json
            CELERY_RESULT_SERIALIZER = json
            CELERY_TIMEZONE = Asia/Shanghai
        

3. **python3 虚拟环境安装**
      ```
        #进入项目根目录
        python3 -m venv env
        source env/bin/activate
        cd install && pip install -r pip_list.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com
        cd ../cmdb
        
        #创建表结构
        #首次执行操作，请忽略,该命令是有表结构更改的时候使用. python manage.py makemigrations
        python manage.py migrate
        #创建超级用户
        python manage.py createsuperuser 
        #如果想修改密码
        python manage.py changepassword
        #启动程序
        进入项目根目录/cmdb
        sh startup.sh start
        #默认启动端口是12000,如果要修改端口，则修改项目根目录/cmdb/startup.sh中port的设定值即可
        #以上命令完成后,则配置和启动完毕
               
      ```


# 使用说明：

1. **故障自愈配置**
      ```
        1> 系统管理配置-配置邮件服务器信息和API权限验证
        2> 首先配置资产用户-连接远程主机的账号密码或者密钥
        3> 配置资产项目-自愈的白名单，支持批量导入,注意csv文件名不能用中文。按照项目根目录/client/demo.csv进行配置
        4> 配置故障自愈-报警后触发的执行动作
        5> 配置联系人和联系组-告警邮件发送
        6> 配置监控项-你监控的目标和要自愈动作的逻辑配置
        7> 第三方日志接入方式: 把项目根目录/client/第三方监控软件报警信息接入.py 中代码集成到nagios 或者zabbix等报警平台报警配置接口脚本中
      ```
      

2. **web展现**

     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/mysql.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/slowsql.png)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/ssh.jpg)   
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/asset.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/auto_recovery.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/monitor_config.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/alert_history.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/tenet.jpg)
     ![Image text](https://gitee.com/haocx/fsr/raw/master/cmdb/pictrue/cpu.jpg)
       
3. **加QQ群**
      ```
       可在群里进行讨论反馈问题
       群号:  817399355
      ```
4. **参考开源项目**
      ```
       django-webssh syscmdb
      ```