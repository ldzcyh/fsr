-- MySQL dump 10.13  Distrib 5.7.30, for el7 (x86_64)
--
-- Host: 10.16.229.187    Database: fsr
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alert_alertconfig`
--

DROP TABLE IF EXISTS `alert_alertconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_alertconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `cname` varchar(30) DEFAULT NULL,
  `apitoken` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `host` (`host`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password` (`password`),
  UNIQUE KEY `cname` (`cname`),
  UNIQUE KEY `apitoken` (`apitoken`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_alertconfig`
--

LOCK TABLES `alert_alertconfig` WRITE;
/*!40000 ALTER TABLE `alert_alertconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_alertconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_alerthistory`
--

DROP TABLE IF EXISTS `alert_alerthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_alerthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(80) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `alertime` datetime(6) DEFAULT NULL,
  `endtime` datetime(6) DEFAULT NULL,
  `startime` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_alerthistory`
--

LOCK TABLES `alert_alerthistory` WRITE;
/*!40000 ALTER TABLE `alert_alerthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_alerthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_linkgroup`
--

DROP TABLE IF EXISTS `alert_linkgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_linkgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `utime` datetime(6) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `dingurl` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_linkgroup`
--

LOCK TABLES `alert_linkgroup` WRITE;
/*!40000 ALTER TABLE `alert_linkgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_linkgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_linkgroup_gl`
--

DROP TABLE IF EXISTS `alert_linkgroup_gl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_linkgroup_gl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linkgroup_id` int(11) NOT NULL,
  `linkperson_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alert_linkgroup_gl_linkgroup_id_linkperson_id_26582b4a_uniq` (`linkgroup_id`,`linkperson_id`),
  KEY `alert_linkgroup_gl_linkperson_id_94a3629b_fk_alert_linkperson_id` (`linkperson_id`),
  CONSTRAINT `alert_linkgroup_gl_linkgroup_id_c6777911_fk_alert_linkgroup_id` FOREIGN KEY (`linkgroup_id`) REFERENCES `alert_linkgroup` (`id`),
  CONSTRAINT `alert_linkgroup_gl_linkperson_id_94a3629b_fk_alert_linkperson_id` FOREIGN KEY (`linkperson_id`) REFERENCES `alert_linkperson` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_linkgroup_gl`
--

LOCK TABLES `alert_linkgroup_gl` WRITE;
/*!40000 ALTER TABLE `alert_linkgroup_gl` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_linkgroup_gl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_linkgroup_ml`
--

DROP TABLE IF EXISTS `alert_linkgroup_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_linkgroup_ml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linkgroup_id` int(11) NOT NULL,
  `monitorconfig_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alert_linkgroup_ml_linkgroup_id_monitorconfig_id_ac842700_uniq` (`linkgroup_id`,`monitorconfig_id`),
  KEY `alert_linkgroup_ml_monitorconfig_id_efe27421_fk_devops_mo` (`monitorconfig_id`),
  CONSTRAINT `alert_linkgroup_ml_linkgroup_id_fdc50540_fk_alert_linkgroup_id` FOREIGN KEY (`linkgroup_id`) REFERENCES `alert_linkgroup` (`id`),
  CONSTRAINT `alert_linkgroup_ml_monitorconfig_id_efe27421_fk_devops_mo` FOREIGN KEY (`monitorconfig_id`) REFERENCES `devops_monitorconfig` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_linkgroup_ml`
--

LOCK TABLES `alert_linkgroup_ml` WRITE;
/*!40000 ALTER TABLE `alert_linkgroup_ml` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_linkgroup_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_linkperson`
--

DROP TABLE IF EXISTS `alert_linkperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_linkperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `dingding` varchar(200) DEFAULT NULL,
  `utime` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_linkperson`
--

LOCK TABLES `alert_linkperson` WRITE;
/*!40000 ALTER TABLE `alert_linkperson` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_linkperson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'添加用户组',6,'add_group'),(17,'删除用户组',6,'delete_group'),(18,'查看用户组',6,'view_group'),(19,'修改用户组',6,'modify_group'),(20,'设置用户组权限',6,'group_set_perm'),(21,'添加用户',7,'add_user'),(22,'删除用户',7,'delete_user'),(23,'查看用户',7,'view_user'),(24,'修改用户',7,'modify_user'),(25,'禁止或允许用户',7,'user_status_stop'),(26,'禁止用户密码',7,'user_set_password'),(27,'设置用户权限',7,'user_set_perm'),(28,'添加机房',10,'add_idc'),(29,'删除机房',10,'delete_idc'),(30,'查看机房',10,'view_idc'),(31,'修改机房',10,'modify_idc'),(32,'查看资产主机',12,'view_server'),(33,'添加资产主机',12,'add_server'),(34,'删除资产主机',12,'delete_server'),(35,'设置资产主机业务线',12,'server_set_product'),(36,'资产主机刷新探测',12,'server_scan_status'),(37,'添加资产用户',14,'add_serveruser'),(38,'删除资产用户',14,'delete_serveruser'),(39,'查看资产用户',14,'view_serveruser'),(40,'修改资产用户',14,'modify_serveruser'),(41,'Can add new server',15,'add_newserver'),(42,'Can change new server',15,'change_newserver'),(43,'Can delete new server',15,'delete_newserver'),(44,'添加业务线',16,'add_product'),(45,'删除业务线',16,'delete_product'),(46,'查看业务线',16,'view_product'),(47,'修改业务线',16,'modify_product'),(48,'资产项目创建机器组',16,'add_product_host'),(49,'Can add file update re card',17,'add_fileupdaterecard'),(50,'Can change file update re card',17,'change_fileupdaterecard'),(51,'Can delete file update re card',17,'delete_fileupdaterecard'),(52,'Can add bao update re card',18,'add_baoupdaterecard'),(53,'Can change bao update re card',18,'change_baoupdaterecard'),(54,'Can delete bao update re card',18,'delete_baoupdaterecard'),(55,'Can add v map',19,'add_vmap'),(56,'Can change v map',19,'change_vmap'),(57,'Can delete v map',19,'delete_vmap'),(58,'Can add assets',20,'add_assets'),(59,'Can change assets',20,'change_assets'),(60,'Can delete assets',20,'delete_assets'),(61,'Can add asset update cards',21,'add_assetupdatecards'),(62,'Can change asset update cards',21,'change_assetupdatecards'),(63,'Can delete asset update cards',21,'delete_assetupdatecards'),(64,'Can add test env detail',22,'add_testenvdetail'),(65,'Can change test env detail',22,'change_testenvdetail'),(66,'Can delete test env detail',22,'delete_testenvdetail'),(67,'Can add sys config file map',23,'add_sysconfigfilemap'),(68,'Can change sys config file map',23,'change_sysconfigfilemap'),(69,'Can delete sys config file map',23,'delete_sysconfigfilemap'),(70,'Can add auto re covery',24,'add_autorecovery'),(71,'Can change auto re covery',24,'change_autorecovery'),(72,'Can delete auto re covery',24,'delete_autorecovery'),(73,'Can add monitor config',25,'add_monitorconfig'),(74,'Can change monitor config',25,'change_monitorconfig'),(75,'Can delete monitor config',25,'delete_monitorconfig'),(76,'Can add crontab',26,'add_crontabschedule'),(77,'Can change crontab',26,'change_crontabschedule'),(78,'Can delete crontab',26,'delete_crontabschedule'),(79,'Can add interval',27,'add_intervalschedule'),(80,'Can change interval',27,'change_intervalschedule'),(81,'Can delete interval',27,'delete_intervalschedule'),(82,'Can add periodic task',28,'add_periodictask'),(83,'Can change periodic task',28,'change_periodictask'),(84,'Can delete periodic task',28,'delete_periodictask'),(85,'Can add periodic tasks',29,'add_periodictasks'),(86,'Can change periodic tasks',29,'change_periodictasks'),(87,'Can delete periodic tasks',29,'delete_periodictasks'),(88,'Can add task state',30,'add_taskmeta'),(89,'Can change task state',30,'change_taskmeta'),(90,'Can delete task state',30,'delete_taskmeta'),(91,'Can add saved group result',31,'add_tasksetmeta'),(92,'Can change saved group result',31,'change_tasksetmeta'),(93,'Can delete saved group result',31,'delete_tasksetmeta'),(94,'Can add task',32,'add_taskstate'),(95,'Can change task',32,'change_taskstate'),(96,'Can delete task',32,'delete_taskstate'),(97,'Can add worker',33,'add_workerstate'),(98,'Can change worker',33,'change_workerstate'),(99,'Can delete worker',33,'delete_workerstate'),(100,'Can add django job execution',34,'add_djangojobexecution'),(101,'Can change django job execution',34,'change_djangojobexecution'),(102,'Can delete django job execution',34,'delete_djangojobexecution'),(103,'Can add django job',35,'add_djangojob'),(104,'Can change django job',35,'change_djangojob'),(105,'Can delete django job',35,'delete_djangojob'),(106,'Can add alert history',36,'add_alerthistory'),(107,'Can change alert history',36,'change_alerthistory'),(108,'Can delete alert history',36,'delete_alerthistory'),(109,'Can add link group',37,'add_linkgroup'),(110,'Can change link group',37,'change_linkgroup'),(111,'Can delete link group',37,'delete_linkgroup'),(112,'Can add link person',38,'add_linkperson'),(113,'Can change link person',38,'change_linkperson'),(114,'Can delete link person',38,'delete_linkperson'),(115,'Can add server group',39,'add_servergroup'),(116,'Can change server group',39,'change_servergroup'),(117,'Can delete server group',39,'delete_servergroup'),(118,'Can add alert config',40,'add_alertconfig'),(119,'Can change alert config',40,'change_alertconfig'),(120,'Can delete alert config',40,'delete_alertconfig'),(121,'Can add chart history',41,'add_charthistory'),(122,'Can change chart history',41,'change_charthistory'),(123,'Can delete chart history',41,'delete_charthistory'),(124,'Can add my sql config',42,'add_mysqlconfig'),(125,'Can change my sql config',42,'change_mysqlconfig'),(126,'Can delete my sql config',42,'delete_mysqlconfig'),(127,'Can add my sql history',43,'add_mysqlhistory'),(128,'Can change my sql history',43,'change_mysqlhistory'),(129,'Can delete my sql history',43,'delete_mysqlhistory'),(130,'Can add slow query review',44,'add_slowqueryreview'),(131,'Can change slow query review',44,'change_slowqueryreview'),(132,'Can delete slow query review',44,'delete_slowqueryreview'),(133,'Can add slow query review history',45,'add_slowqueryreviewhistory'),(134,'Can change slow query review history',45,'change_slowqueryreviewhistory'),(135,'Can delete slow query review history',45,'delete_slowqueryreviewhistory'),(136,'Can add oracle config',46,'add_oracleconfig'),(137,'Can change oracle config',46,'change_oracleconfig'),(138,'Can delete oracle config',46,'delete_oracleconfig'),(139,'Can add oracle history',47,'add_oraclehistory'),(140,'Can change oracle history',47,'change_oraclehistory'),(141,'Can delete oracle history',47,'delete_oraclehistory'),(142,'Can add web services',48,'add_webservices'),(143,'Can change web services',48,'change_webservices'),(144,'Can delete web services',48,'delete_webservices');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (22,'pbkdf2_sha256$100000$welzi9YybXU6$S/pez0AuCybX73pBjqoNTwRAmFaYl/h3LYGTY2ycISA=','2022-09-23 10:57:07.736396',1,'admin','','','',1,1,'2022-01-06 16:57:49.684738');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_taskmeta`
--

DROP TABLE IF EXISTS `celery_taskmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` longtext,
  `date_done` datetime(6) NOT NULL,
  `traceback` longtext,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `celery_taskmeta_hidden_23fd02dc` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_taskmeta`
--

LOCK TABLES `celery_taskmeta` WRITE;
/*!40000 ALTER TABLE `celery_taskmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_taskmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_tasksetmeta`
--

DROP TABLE IF EXISTS `celery_tasksetmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) NOT NULL,
  `result` longtext NOT NULL,
  `date_done` datetime(6) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskset_id` (`taskset_id`),
  KEY `celery_tasksetmeta_hidden_593cfc24` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_tasksetmeta`
--

LOCK TABLES `celery_tasksetmeta` WRITE;
/*!40000 ALTER TABLE `celery_tasksetmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_tasksetmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbmonitor_mysqlconfig`
--

DROP TABLE IF EXISTS `dbmonitor_mysqlconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbmonitor_mysqlconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `ip` varchar(80) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `dbname` varchar(80) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `utime` datetime(6) DEFAULT NULL,
  `mysql_master_slave` int(11) DEFAULT NULL,
  `Slave_IO_Running` int(11) DEFAULT NULL,
  `Slave_SQL_Running` int(11) DEFAULT NULL,
  `charset` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dbname` (`dbname`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbmonitor_mysqlconfig`
--

LOCK TABLES `dbmonitor_mysqlconfig` WRITE;
/*!40000 ALTER TABLE `dbmonitor_mysqlconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbmonitor_mysqlconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbmonitor_mysqlhistory`
--

DROP TABLE IF EXISTS `dbmonitor_mysqlhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbmonitor_mysqlhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mysqlid` int(11) DEFAULT NULL,
  `total_qps` int(11) DEFAULT NULL,
  `insert_qps` int(11) DEFAULT NULL,
  `update_qps` int(11) DEFAULT NULL,
  `select_qps` int(11) DEFAULT NULL,
  `delete_qps` int(11) DEFAULT NULL,
  `commit_tps` int(11) DEFAULT NULL,
  `rollback_tps` int(11) DEFAULT NULL,
  `utime` datetime(6) DEFAULT NULL,
  `threads_cached` int(11) DEFAULT NULL,
  `threads_connected` int(11) DEFAULT NULL,
  `threads_created` int(11) DEFAULT NULL,
  `threads_running` int(11) DEFAULT NULL,
  `bytes_received` int(11) DEFAULT NULL,
  `bytes_sent` int(11) DEFAULT NULL,
  `Innodb_buffer_pool_pages_flushed` int(11) DEFAULT NULL,
  `innodb_buffer_pool_reads` int(11) DEFAULT NULL,
  `key_blocks_used_rate` double DEFAULT NULL,
  `key_buffer_read_rate` double DEFAULT NULL,
  `key_buffer_write_rate` double DEFAULT NULL,
  `Exec_Master_Log_Pos` int(11) DEFAULT NULL,
  `Read_Master_Log_Pos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dbmonitor_m_utime_5607c4_idx` (`utime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbmonitor_mysqlhistory`
--

LOCK TABLES `dbmonitor_mysqlhistory` WRITE;
/*!40000 ALTER TABLE `dbmonitor_mysqlhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbmonitor_mysqlhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbmonitor_oracleconfig`
--

DROP TABLE IF EXISTS `dbmonitor_oracleconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbmonitor_oracleconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbname` varchar(80) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `ip` varchar(80) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `health_status` tinyint(1) NOT NULL,
  `utime` datetime(6) DEFAULT NULL,
  `case` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dbname` (`dbname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbmonitor_oracleconfig`
--

LOCK TABLES `dbmonitor_oracleconfig` WRITE;
/*!40000 ALTER TABLE `dbmonitor_oracleconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbmonitor_oracleconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbmonitor_oraclehistory`
--

DROP TABLE IF EXISTS `dbmonitor_oraclehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbmonitor_oraclehistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oracleid` int(11) DEFAULT NULL,
  `tablespace` longtext,
  `utime` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbmonitor_oraclehistory`
--

LOCK TABLES `dbmonitor_oraclehistory` WRITE;
/*!40000 ALTER TABLE `dbmonitor_oraclehistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbmonitor_oraclehistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbmonitor_slowqueryreview`
--

DROP TABLE IF EXISTS `dbmonitor_slowqueryreview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbmonitor_slowqueryreview` (
  `checksum` bigint(20) unsigned NOT NULL,
  `fingerprint` text NOT NULL,
  `sample` text NOT NULL,
  `first_seen` datetime DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `reviewed_by` varchar(20) DEFAULT NULL,
  `reviewed_on` datetime DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`checksum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbmonitor_slowqueryreview`
--

LOCK TABLES `dbmonitor_slowqueryreview` WRITE;
/*!40000 ALTER TABLE `dbmonitor_slowqueryreview` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbmonitor_slowqueryreview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbmonitor_slowqueryreviewhistory`
--

DROP TABLE IF EXISTS `dbmonitor_slowqueryreviewhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbmonitor_slowqueryreviewhistory` (
  `serverid_max` smallint(4) NOT NULL DEFAULT '0',
  `db_max` varchar(100) DEFAULT NULL,
  `user_max` varchar(100) DEFAULT NULL,
  `checksum` bigint(20) unsigned NOT NULL,
  `sample` text NOT NULL,
  `ts_min` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ts_max` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ts_cnt` float DEFAULT NULL,
  `Query_time_sum` float DEFAULT NULL,
  `Query_time_min` float DEFAULT NULL,
  `Query_time_max` float DEFAULT NULL,
  `Query_time_pct_95` float DEFAULT NULL,
  `Query_time_stddev` float DEFAULT NULL,
  `Query_time_median` float DEFAULT NULL,
  `Lock_time_sum` float DEFAULT NULL,
  `Lock_time_min` float DEFAULT NULL,
  `Lock_time_max` float DEFAULT NULL,
  `Lock_time_pct_95` float DEFAULT NULL,
  `Lock_time_stddev` float DEFAULT NULL,
  `Lock_time_median` float DEFAULT NULL,
  `Rows_sent_sum` float DEFAULT NULL,
  `Rows_sent_min` float DEFAULT NULL,
  `Rows_sent_max` float DEFAULT NULL,
  `Rows_sent_pct_95` float DEFAULT NULL,
  `Rows_sent_stddev` float DEFAULT NULL,
  `Rows_sent_median` float DEFAULT NULL,
  `Rows_examined_sum` float DEFAULT NULL,
  `Rows_examined_min` float DEFAULT NULL,
  `Rows_examined_max` float DEFAULT NULL,
  `Rows_examined_pct_95` float DEFAULT NULL,
  `Rows_examined_stddev` float DEFAULT NULL,
  `Rows_examined_median` float DEFAULT NULL,
  `Rows_affected_sum` float DEFAULT NULL,
  `Rows_affected_min` float DEFAULT NULL,
  `Rows_affected_max` float DEFAULT NULL,
  `Rows_affected_pct_95` float DEFAULT NULL,
  `Rows_affected_stddev` float DEFAULT NULL,
  `Rows_affected_median` float DEFAULT NULL,
  `Rows_read_sum` float DEFAULT NULL,
  `Rows_read_min` float DEFAULT NULL,
  `Rows_read_max` float DEFAULT NULL,
  `Rows_read_pct_95` float DEFAULT NULL,
  `Rows_read_stddev` float DEFAULT NULL,
  `Rows_read_median` float DEFAULT NULL,
  `Merge_passes_sum` float DEFAULT NULL,
  `Merge_passes_min` float DEFAULT NULL,
  `Merge_passes_max` float DEFAULT NULL,
  `Merge_passes_pct_95` float DEFAULT NULL,
  `Merge_passes_stddev` float DEFAULT NULL,
  `Merge_passes_median` float DEFAULT NULL,
  `InnoDB_IO_r_ops_min` float DEFAULT NULL,
  `InnoDB_IO_r_ops_max` float DEFAULT NULL,
  `InnoDB_IO_r_ops_pct_95` float DEFAULT NULL,
  `InnoDB_IO_r_ops_stddev` float DEFAULT NULL,
  `InnoDB_IO_r_ops_median` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_min` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_max` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_pct_95` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_stddev` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_median` float DEFAULT NULL,
  `InnoDB_IO_r_wait_min` float DEFAULT NULL,
  `InnoDB_IO_r_wait_max` float DEFAULT NULL,
  `InnoDB_IO_r_wait_pct_95` float DEFAULT NULL,
  `InnoDB_IO_r_wait_stddev` float DEFAULT NULL,
  `InnoDB_IO_r_wait_median` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_min` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_max` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_pct_95` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_stddev` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_median` float DEFAULT NULL,
  `InnoDB_queue_wait_min` float DEFAULT NULL,
  `InnoDB_queue_wait_max` float DEFAULT NULL,
  `InnoDB_queue_wait_pct_95` float DEFAULT NULL,
  `InnoDB_queue_wait_stddev` float DEFAULT NULL,
  `InnoDB_queue_wait_median` float DEFAULT NULL,
  `InnoDB_pages_distinct_min` float DEFAULT NULL,
  `InnoDB_pages_distinct_max` float DEFAULT NULL,
  `InnoDB_pages_distinct_pct_95` float DEFAULT NULL,
  `InnoDB_pages_distinct_stddev` float DEFAULT NULL,
  `InnoDB_pages_distinct_median` float DEFAULT NULL,
  `QC_Hit_cnt` float DEFAULT NULL,
  `QC_Hit_sum` float DEFAULT NULL,
  `Full_scan_cnt` float DEFAULT NULL,
  `Full_scan_sum` float DEFAULT NULL,
  `Full_join_cnt` float DEFAULT NULL,
  `Full_join_sum` float DEFAULT NULL,
  `Tmp_table_cnt` float DEFAULT NULL,
  `Tmp_table_sum` float DEFAULT NULL,
  `Tmp_table_on_disk_cnt` float DEFAULT NULL,
  `Tmp_table_on_disk_sum` float DEFAULT NULL,
  `Filesort_cnt` float DEFAULT NULL,
  `Filesort_sum` float DEFAULT NULL,
  `Filesort_on_disk_cnt` float DEFAULT NULL,
  `Filesort_on_disk_sum` float DEFAULT NULL,
  PRIMARY KEY (`checksum`,`ts_min`,`ts_max`),
  KEY `idx_serverid_max` (`serverid_max`) USING BTREE,
  KEY `idx_checksum` (`checksum`) USING BTREE,
  KEY `idx_query_time_max` (`Query_time_max`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbmonitor_slowqueryreviewhistory`
--

LOCK TABLES `dbmonitor_slowqueryreviewhistory` WRITE;
/*!40000 ALTER TABLE `dbmonitor_slowqueryreviewhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbmonitor_slowqueryreviewhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devops_autorecovery`
--

DROP TABLE IF EXISTS `devops_autorecovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devops_autorecovery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(80) DEFAULT NULL,
  `action` varchar(2000) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `INDEX_NAME` (`item`),
  UNIQUE KEY `devops_autorecovery_item_0e929153_uniq` (`item`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devops_autorecovery`
--

LOCK TABLES `devops_autorecovery` WRITE;
/*!40000 ALTER TABLE `devops_autorecovery` DISABLE KEYS */;
/*!40000 ALTER TABLE `devops_autorecovery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devops_autorecovery_mc`
--

DROP TABLE IF EXISTS `devops_autorecovery_mc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devops_autorecovery_mc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autorecovery_id` int(11) NOT NULL,
  `monitorconfig_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `devops_autorecovery_mc_autorecovery_id_monitorc_9549493b_uniq` (`autorecovery_id`,`monitorconfig_id`),
  KEY `devops_autorecovery__monitorconfig_id_30bf17dc_fk_devops_mo` (`monitorconfig_id`),
  CONSTRAINT `devops_autorecovery__autorecovery_id_5dc7a503_fk_devops_au` FOREIGN KEY (`autorecovery_id`) REFERENCES `devops_autorecovery` (`id`),
  CONSTRAINT `devops_autorecovery__monitorconfig_id_30bf17dc_fk_devops_mo` FOREIGN KEY (`monitorconfig_id`) REFERENCES `devops_monitorconfig` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devops_autorecovery_mc`
--

LOCK TABLES `devops_autorecovery_mc` WRITE;
/*!40000 ALTER TABLE `devops_autorecovery_mc` DISABLE KEYS */;
/*!40000 ALTER TABLE `devops_autorecovery_mc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devops_charthistory`
--

DROP TABLE IF EXISTS `devops_charthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devops_charthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostid` int(11) DEFAULT NULL,
  `cpu` double DEFAULT NULL,
  `mem` double DEFAULT NULL,
  `process` int(11) DEFAULT NULL,
  `utime` datetime(6) DEFAULT NULL,
  `disk` longtext,
  `nflow` longtext,
  PRIMARY KEY (`id`),
  KEY `devops_char_utime_39e175_idx` (`utime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devops_charthistory`
--

LOCK TABLES `devops_charthistory` WRITE;
/*!40000 ALTER TABLE `devops_charthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `devops_charthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devops_monitorconfig`
--

DROP TABLE IF EXISTS `devops_monitorconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devops_monitorconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `utime` datetime(6) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `alert_status` int(11) DEFAULT NULL,
  `auto_status` tinyint(1) NOT NULL,
  `alert_type` int(11) DEFAULT NULL,
  `sv_ip` varchar(500) DEFAULT NULL,
  `process_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devops_monitorconfig`
--

LOCK TABLES `devops_monitorconfig` WRITE;
/*!40000 ALTER TABLE `devops_monitorconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `devops_monitorconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_apscheduler_djangojob`
--

DROP TABLE IF EXISTS `django_apscheduler_djangojob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_apscheduler_djangojob` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `next_run_time` datetime(6) DEFAULT NULL,
  `job_state` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `django_apscheduler_djangojob_next_run_time_2f022619` (`next_run_time`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_apscheduler_djangojob`
--

LOCK TABLES `django_apscheduler_djangojob` WRITE;
/*!40000 ALTER TABLE `django_apscheduler_djangojob` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_apscheduler_djangojob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_apscheduler_djangojobexecution`
--

DROP TABLE IF EXISTS `django_apscheduler_djangojobexecution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_apscheduler_djangojobexecution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `run_time` datetime(6) NOT NULL,
  `duration` decimal(15,2) DEFAULT NULL,
  `started` decimal(15,2) DEFAULT NULL,
  `finished` decimal(15,2) DEFAULT NULL,
  `exception` varchar(1000) DEFAULT NULL,
  `traceback` longtext,
  `job_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_apscheduler_d_job_id_daf5090a_fk_django_ap` (`job_id`),
  KEY `django_apscheduler_djangojobexecution_run_time_16edd96b` (`run_time`),
  CONSTRAINT `django_apscheduler_d_job_id_daf5090a_fk_django_ap` FOREIGN KEY (`job_id`) REFERENCES `django_apscheduler_djangojob` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_apscheduler_djangojobexecution`
--

LOCK TABLES `django_apscheduler_djangojobexecution` WRITE;
/*!40000 ALTER TABLE `django_apscheduler_djangojobexecution` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_apscheduler_djangojobexecution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (40,'alert','alertconfig'),(36,'alert','alerthistory'),(37,'alert','linkgroup'),(38,'alert','linkperson'),(2,'auth','group'),(1,'auth','permission'),(3,'auth','user'),(4,'contenttypes','contenttype'),(42,'dbmonitor','mysqlconfig'),(43,'dbmonitor','mysqlhistory'),(46,'dbmonitor','oracleconfig'),(47,'dbmonitor','oraclehistory'),(44,'dbmonitor','slowqueryreview'),(45,'dbmonitor','slowqueryreviewhistory'),(20,'devops','assets'),(21,'devops','assetupdatecards'),(24,'devops','autorecovery'),(18,'devops','baoupdaterecard'),(41,'devops','charthistory'),(17,'devops','fileupdaterecard'),(25,'devops','monitorconfig'),(23,'devops','sysconfigfilemap'),(22,'devops','testenvdetail'),(19,'devops','vmap'),(35,'django_apscheduler','djangojob'),(34,'django_apscheduler','djangojobexecution'),(26,'djcelery','crontabschedule'),(27,'djcelery','intervalschedule'),(28,'djcelery','periodictask'),(29,'djcelery','periodictasks'),(30,'djcelery','taskmeta'),(31,'djcelery','tasksetmeta'),(32,'djcelery','taskstate'),(33,'djcelery','workerstate'),(48,'oneweb','webservices'),(16,'products','product'),(9,'resources','disk'),(10,'resources','idc'),(11,'resources','ip'),(15,'resources','newserver'),(12,'resources','server'),(13,'resources','serverauto'),(39,'resources','servergroup'),(14,'resources','serveruser'),(5,'sessions','session'),(6,'users','groupprofile'),(7,'users','profile'),(8,'users','registeremail');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2021-07-02 11:08:54.959357'),(2,'contenttypes','0002_remove_content_type_name','2021-07-02 11:08:55.103697'),(3,'auth','0001_initial','2021-07-02 11:08:56.592841'),(4,'auth','0002_alter_permission_name_max_length','2021-07-02 11:08:56.730884'),(5,'auth','0003_alter_user_email_max_length','2021-07-02 11:08:56.849750'),(6,'auth','0004_alter_user_username_opts','2021-07-02 11:08:56.865485'),(7,'auth','0005_alter_user_last_login_null','2021-07-02 11:08:56.965630'),(8,'auth','0006_require_contenttypes_0002','2021-07-02 11:08:56.971223'),(9,'auth','0007_alter_validators_add_error_messages','2021-07-02 11:08:56.988416'),(10,'auth','0008_alter_user_username_max_length','2021-07-02 11:08:57.014962'),(11,'devops','0001_initial','2021-07-02 11:08:57.068239'),(12,'devops','0002_auto_20190328_1544','2021-07-02 11:08:57.167481'),(13,'devops','0003_baoupdaterecard','2021-07-02 11:08:57.245913'),(14,'devops','0004_vmap','2021-07-02 11:08:57.430375'),(15,'devops','0005_auto_20200217_0952','2021-07-02 11:08:57.504800'),(16,'devops','0006_auto_20200407_1057','2021-07-02 11:08:57.545730'),(17,'devops','0007_assets_k8s','2021-07-02 11:08:57.584830'),(18,'devops','0008_auto_20200602_1244','2021-07-02 11:08:57.599292'),(19,'devops','0009_testenvdetail','2021-07-02 11:08:57.694880'),(20,'devops','0010_auto_20210604_1713','2021-07-02 11:08:57.716359'),(21,'devops','0011_sysconfigfilemap','2021-07-02 11:08:57.753198'),(22,'devops','0012_auto_20210629_1413','2021-07-02 11:08:57.774975'),(23,'products','0001_initial','2021-07-02 11:08:57.860253'),(24,'resources','0001_initial','2021-07-02 11:08:59.518108'),(25,'resources','0002_newserver','2021-07-02 11:08:59.567291'),(26,'sessions','0001_initial','2021-07-02 11:08:59.757456'),(27,'users','0001_initial','2021-07-02 11:09:00.159933'),(28,'users','0002_auto_20190403_1147','2021-07-02 11:09:00.184061'),(29,'users','0003_profile_experm','2021-07-02 11:09:00.220615'),(30,'resources','0003_newserver_server_user','2021-07-05 13:41:53.782171'),(31,'devops','0013_autorecovery','2021-07-05 16:48:51.734944'),(32,'devops','0014_auto_20210707_1422','2021-07-07 14:22:46.665042'),(33,'devops','0015_monitorconfig_remarks','2021-07-07 15:16:23.895364'),(34,'devops','0016_auto_20210707_1534','2021-07-07 15:34:31.448880'),(35,'devops','0017_auto_20210707_1556','2021-07-07 15:56:40.908698'),(36,'devops','0018_auto_20210707_2114','2021-07-07 21:15:01.795420'),(37,'djcelery','0001_initial','2021-07-08 18:10:19.678767'),(38,'django_apscheduler','0001_initial','2021-07-14 13:53:54.603462'),(39,'django_apscheduler','0002_auto_20180412_0758','2021-07-14 14:26:03.451983'),(40,'devops','0019_monitorconfig_at_mc','2021-07-17 18:21:49.749976'),(41,'alert','0001_initial','2021-07-19 14:09:17.621406'),(42,'devops','0020_monitorconfig_alert_status','2021-07-19 14:24:10.801241'),(43,'alert','0002_auto_20210719_1555','2021-07-19 15:55:38.985668'),(44,'alert','0003_auto_20210720_1000','2021-07-20 10:00:09.246298'),(45,'devops','0021_auto_20210720_1000','2021-07-20 10:00:09.262677'),(46,'alert','0004_auto_20210720_1012','2021-07-20 10:12:32.349976'),(47,'alert','0005_auto_20210720_1139','2021-07-20 11:39:25.540500'),(48,'alert','0006_linkgroup_info','2021-07-20 15:15:06.076295'),(49,'alert','0007_auto_20210721_0932','2021-07-21 09:32:58.860279'),(50,'alert','0008_auto_20210721_1132','2021-07-21 11:32:05.207382'),(51,'alert','0009_auto_20210722_0949','2021-07-22 09:49:37.252767'),(52,'devops','0022_autorecovery_status','2021-07-26 15:45:50.339998'),(53,'devops','0023_autorecovery_auto_status','2021-07-26 15:45:50.370716'),(54,'resources','0004_auto_20210722_1705','2021-07-26 15:51:45.368864'),(55,'resources','0005_auto_20210723_1055','2021-07-26 15:51:45.533497'),(56,'alert','0010_auto_20210727_1342','2021-07-27 13:43:14.365824'),(57,'devops','0024_auto_20210727_1452','2021-07-27 14:52:30.950449'),(58,'devops','0025_auto_20210727_1522','2021-07-27 15:51:27.940692'),(59,'alert','0011_linkgroup_ml','2021-07-27 15:51:28.190711'),(60,'devops','0026_monitorconfig_auto_status','2021-07-27 15:52:11.298575'),(61,'alert','0012_auto_20210727_1554','2021-07-27 15:54:09.286172'),(62,'alert','0013_alertconfig','2021-07-28 11:03:53.431948'),(63,'alert','0014_auto_20210728_1104','2021-07-28 11:04:49.672615'),(64,'alert','0015_auto_20210728_1359','2021-07-28 13:59:47.502087'),(65,'alert','0016_auto_20210728_1408','2021-07-28 14:08:48.027936'),(66,'devops','0027_monitorconfig_alert_type','2021-07-28 14:29:17.445109'),(67,'resources','0006_serveruser_privatekey','2021-07-29 14:07:16.308075'),(68,'alert','0017_auto_20210730_1918','2021-07-30 19:18:58.493681'),(69,'alert','0018_auto_20210805_1102','2021-08-05 11:03:17.493352'),(70,'auth','0009_alter_user_last_name_max_length','2021-08-05 11:03:17.531878'),(71,'devops','0028_auto_20210805_1102','2021-08-05 11:03:17.571058'),(72,'resources','0007_auto_20210805_1102','2021-08-05 11:03:18.161591'),(73,'users','0004_auto_20210805_1102','2021-08-05 11:03:18.182954'),(74,'resources','0008_auto_20210818_1136','2021-08-18 11:37:10.405477'),(75,'resources','0009_auto_20210818_1415','2021-08-18 14:15:12.158777'),(76,'devops','0029_charthistory','2021-08-19 15:30:17.989511'),(77,'devops','0030_charthistory_utime','2021-08-19 16:00:42.754107'),(78,'devops','0031_auto_20210819_1704','2021-08-19 17:04:50.903148'),(79,'devops','0032_auto_20210820_1101','2021-08-20 11:01:22.371835'),(80,'devops','0033_auto_20210820_1105','2021-08-20 11:06:00.728209'),(81,'devops','0034_charthistory_disk_root','2021-08-23 11:07:42.883407'),(82,'dbmonitor','0001_initial','2021-08-26 10:40:31.590689'),(83,'dbmonitor','0002_auto_20210826_1102','2021-08-26 11:02:23.645043'),(84,'dbmonitor','0003_auto_20210826_1105','2021-08-26 11:05:57.153720'),(85,'dbmonitor','0004_mysqlhistory','2021-08-26 16:42:15.801881'),(86,'dbmonitor','0005_auto_20210829_0931','2021-08-29 09:31:26.933153'),(87,'dbmonitor','0006_auto_20210829_1333','2021-08-29 13:33:18.891410'),(88,'dbmonitor','0007_auto_20210829_1743','2021-08-29 17:43:43.654502'),(89,'dbmonitor','0008_auto_20210829_1902','2021-08-29 19:02:21.630492'),(90,'dbmonitor','0009_auto_20210903_1149','2021-09-03 11:49:40.359893'),(91,'dbmonitor','0010_auto_20210903_1149','2021-09-03 11:49:40.444189'),(92,'dbmonitor','0011_mysqlconfig_mysql_master_slave','2021-09-03 15:48:02.686990'),(93,'dbmonitor','0012_auto_20210903_1554','2021-09-03 15:54:28.390219'),(94,'dbmonitor','0013_auto_20210903_1608','2021-09-03 16:08:49.319621'),(95,'dbmonitor','0014_auto_20210909_1111','2021-09-09 11:11:21.902392'),(96,'dbmonitor','0015_auto_20210909_1112','2021-09-09 11:12:58.645691'),(97,'dbmonitor','0016_auto_20210909_1113','2021-09-09 11:13:54.125093'),(98,'dbmonitor','0017_delete_slowqueryreview','2021-09-09 11:18:56.427127'),(99,'dbmonitor','0018_auto_20210909_1119','2021-09-09 11:19:41.768442'),(100,'dbmonitor','0019_delete_slowqueryreview','2021-09-09 11:22:58.161988'),(101,'dbmonitor','0020_auto_20210909_1123','2021-09-09 11:23:28.756467'),(102,'dbmonitor','0021_delete_slowqueryreview','2021-09-09 11:26:37.646815'),(103,'dbmonitor','0022_auto_20210909_1126','2021-09-09 11:26:49.943165'),(104,'dbmonitor','0023_auto_20210909_1127','2021-09-09 11:27:46.752842'),(105,'dbmonitor','0024_auto_20210909_1128','2021-09-09 11:28:31.045648'),(106,'dbmonitor','0025_delete_slowqueryreview','2021-09-09 11:31:15.681754'),(107,'dbmonitor','0026_auto_20210909_1137','2021-09-09 11:37:59.424309'),(108,'dbmonitor','0027_auto_20210909_1138','2021-09-09 11:38:24.279120'),(109,'dbmonitor','0028_auto_20210909_1143','2021-09-09 11:43:36.234504'),(110,'dbmonitor','0029_auto_20210909_1152','2021-09-09 11:52:33.483367'),(111,'dbmonitor','0030_auto_20210909_1331','2021-09-09 13:31:59.468457'),(112,'dbmonitor','0031_auto_20210909_1519','2021-09-09 15:19:39.847785'),(113,'devops','0035_auto_20210915_1116','2021-09-15 11:16:59.713693'),(114,'dbmonitor','0032_oracleconfig','2021-09-24 10:40:07.140025'),(115,'dbmonitor','0033_auto_20210924_1130','2021-09-24 11:30:09.602166'),(116,'dbmonitor','0034_auto_20210924_1130','2021-09-24 11:30:55.924985'),(117,'dbmonitor','0035_oraclehistory','2021-09-24 12:18:17.051341'),(118,'devops','0036_auto_20211122_1022','2021-11-22 10:26:06.896332'),(119,'oneweb','0001_initial','2021-11-22 10:26:07.309278'),(120,'oneweb','0002_auto_20211122_1624','2021-11-22 16:24:46.627674'),(121,'devops','0036_auto_20211125_1138','2021-11-25 11:38:53.397994'),(122,'resources','0010_serveruser_pubkey','2021-11-25 11:38:53.504186'),(123,'devops','0037_auto_20211130_1530','2021-11-30 15:30:57.748212'),(124,'devops','0038_auto_20211201_1355','2021-12-01 13:56:02.978332'),(125,'alert','0019_auto_20211214_0934','2021-12-14 09:34:13.940016'),(126,'dbmonitor','0036_auto_20211214_0934','2021-12-14 09:34:16.582185'),(127,'devops','0039_auto_20211214_0936','2021-12-14 09:36:58.602222'),(128,'devops','0040_auto_20211215_1116','2021-12-15 11:16:28.553462'),(129,'alert','0020_auto_20220423_1108','2022-04-23 11:08:24.559927'),(130,'devops','0041_charthistory_nflow','2022-07-02 15:20:11.783391'),(131,'resources','0011_auto_20220819_1108','2022-08-19 11:09:01.508930'),(132,'users','0005_remove_profile_experm','2022-08-19 11:09:01.559291');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_crontabschedule`
--

DROP TABLE IF EXISTS `djcelery_crontabschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) NOT NULL,
  `hour` varchar(64) NOT NULL,
  `day_of_week` varchar(64) NOT NULL,
  `day_of_month` varchar(64) NOT NULL,
  `month_of_year` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_crontabschedule`
--

LOCK TABLES `djcelery_crontabschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_crontabschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_crontabschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_intervalschedule`
--

DROP TABLE IF EXISTS `djcelery_intervalschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_intervalschedule`
--

LOCK TABLES `djcelery_intervalschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_intervalschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_intervalschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictask`
--

DROP TABLE IF EXISTS `djcelery_periodictask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queue` varchar(200) DEFAULT NULL,
  `exchange` varchar(200) DEFAULT NULL,
  `routing_key` varchar(200) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime(6) DEFAULT NULL,
  `total_run_count` int(10) unsigned NOT NULL,
  `date_changed` datetime(6) NOT NULL,
  `description` longtext NOT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `interval_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` (`crontab_id`),
  KEY `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` (`interval_id`),
  CONSTRAINT `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`),
  CONSTRAINT `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictask`
--

LOCK TABLES `djcelery_periodictask` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictask` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictasks`
--

DROP TABLE IF EXISTS `djcelery_periodictasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime(6) NOT NULL,
  PRIMARY KEY (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictasks`
--

LOCK TABLES `djcelery_periodictasks` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_taskstate`
--

DROP TABLE IF EXISTS `djcelery_taskstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tstamp` datetime(6) NOT NULL,
  `args` longtext,
  `kwargs` longtext,
  `eta` datetime(6) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `result` longtext,
  `traceback` longtext,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `djcelery_taskstate_state_53543be4` (`state`),
  KEY `djcelery_taskstate_name_8af9eded` (`name`),
  KEY `djcelery_taskstate_tstamp_4c3f93a1` (`tstamp`),
  KEY `djcelery_taskstate_hidden_c3905e57` (`hidden`),
  KEY `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` (`worker_id`),
  CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_taskstate`
--

LOCK TABLES `djcelery_taskstate` WRITE;
/*!40000 ALTER TABLE `djcelery_taskstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_taskstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_workerstate`
--

DROP TABLE IF EXISTS `djcelery_workerstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `last_heartbeat` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `djcelery_workerstate_last_heartbeat_4539b544` (`last_heartbeat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_workerstate`
--

LOCK TABLES `djcelery_workerstate` WRITE;
/*!40000 ALTER TABLE `djcelery_workerstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_workerstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mysql_slow_query_review`
--

DROP TABLE IF EXISTS `mysql_slow_query_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mysql_slow_query_review` (
  `serverid_max` smallint(4) NOT NULL DEFAULT '0',
  `checksum` bigint(20) unsigned NOT NULL,
  `fingerprint` text NOT NULL,
  `sample` text NOT NULL,
  `first_seen` datetime DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `reviewed_by` varchar(20) DEFAULT NULL,
  `reviewed_on` datetime DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`checksum`),
  KEY `idx_checksum` (`checksum`) USING BTREE,
  KEY `idx_last_seen` (`last_seen`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mysql_slow_query_review`
--

LOCK TABLES `mysql_slow_query_review` WRITE;
/*!40000 ALTER TABLE `mysql_slow_query_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_slow_query_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mysql_slow_query_review_history`
--

DROP TABLE IF EXISTS `mysql_slow_query_review_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mysql_slow_query_review_history` (
  `serverid_max` smallint(4) NOT NULL DEFAULT '0',
  `db_max` varchar(100) DEFAULT NULL,
  `user_max` varchar(100) DEFAULT NULL,
  `checksum` bigint(20) unsigned NOT NULL,
  `sample` text NOT NULL,
  `ts_min` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ts_max` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ts_cnt` float DEFAULT NULL,
  `Query_time_sum` float DEFAULT NULL,
  `Query_time_min` float DEFAULT NULL,
  `Query_time_max` float DEFAULT NULL,
  `Query_time_pct_95` float DEFAULT NULL,
  `Query_time_stddev` float DEFAULT NULL,
  `Query_time_median` float DEFAULT NULL,
  `Lock_time_sum` float DEFAULT NULL,
  `Lock_time_min` float DEFAULT NULL,
  `Lock_time_max` float DEFAULT NULL,
  `Lock_time_pct_95` float DEFAULT NULL,
  `Lock_time_stddev` float DEFAULT NULL,
  `Lock_time_median` float DEFAULT NULL,
  `Rows_sent_sum` float DEFAULT NULL,
  `Rows_sent_min` float DEFAULT NULL,
  `Rows_sent_max` float DEFAULT NULL,
  `Rows_sent_pct_95` float DEFAULT NULL,
  `Rows_sent_stddev` float DEFAULT NULL,
  `Rows_sent_median` float DEFAULT NULL,
  `Rows_examined_sum` float DEFAULT NULL,
  `Rows_examined_min` float DEFAULT NULL,
  `Rows_examined_max` float DEFAULT NULL,
  `Rows_examined_pct_95` float DEFAULT NULL,
  `Rows_examined_stddev` float DEFAULT NULL,
  `Rows_examined_median` float DEFAULT NULL,
  `Rows_affected_sum` float DEFAULT NULL,
  `Rows_affected_min` float DEFAULT NULL,
  `Rows_affected_max` float DEFAULT NULL,
  `Rows_affected_pct_95` float DEFAULT NULL,
  `Rows_affected_stddev` float DEFAULT NULL,
  `Rows_affected_median` float DEFAULT NULL,
  `Rows_read_sum` float DEFAULT NULL,
  `Rows_read_min` float DEFAULT NULL,
  `Rows_read_max` float DEFAULT NULL,
  `Rows_read_pct_95` float DEFAULT NULL,
  `Rows_read_stddev` float DEFAULT NULL,
  `Rows_read_median` float DEFAULT NULL,
  `Merge_passes_sum` float DEFAULT NULL,
  `Merge_passes_min` float DEFAULT NULL,
  `Merge_passes_max` float DEFAULT NULL,
  `Merge_passes_pct_95` float DEFAULT NULL,
  `Merge_passes_stddev` float DEFAULT NULL,
  `Merge_passes_median` float DEFAULT NULL,
  `InnoDB_IO_r_ops_min` float DEFAULT NULL,
  `InnoDB_IO_r_ops_max` float DEFAULT NULL,
  `InnoDB_IO_r_ops_pct_95` float DEFAULT NULL,
  `InnoDB_IO_r_ops_stddev` float DEFAULT NULL,
  `InnoDB_IO_r_ops_median` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_min` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_max` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_pct_95` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_stddev` float DEFAULT NULL,
  `InnoDB_IO_r_bytes_median` float DEFAULT NULL,
  `InnoDB_IO_r_wait_min` float DEFAULT NULL,
  `InnoDB_IO_r_wait_max` float DEFAULT NULL,
  `InnoDB_IO_r_wait_pct_95` float DEFAULT NULL,
  `InnoDB_IO_r_wait_stddev` float DEFAULT NULL,
  `InnoDB_IO_r_wait_median` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_min` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_max` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_pct_95` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_stddev` float DEFAULT NULL,
  `InnoDB_rec_lock_wait_median` float DEFAULT NULL,
  `InnoDB_queue_wait_min` float DEFAULT NULL,
  `InnoDB_queue_wait_max` float DEFAULT NULL,
  `InnoDB_queue_wait_pct_95` float DEFAULT NULL,
  `InnoDB_queue_wait_stddev` float DEFAULT NULL,
  `InnoDB_queue_wait_median` float DEFAULT NULL,
  `InnoDB_pages_distinct_min` float DEFAULT NULL,
  `InnoDB_pages_distinct_max` float DEFAULT NULL,
  `InnoDB_pages_distinct_pct_95` float DEFAULT NULL,
  `InnoDB_pages_distinct_stddev` float DEFAULT NULL,
  `InnoDB_pages_distinct_median` float DEFAULT NULL,
  `QC_Hit_cnt` float DEFAULT NULL,
  `QC_Hit_sum` float DEFAULT NULL,
  `Full_scan_cnt` float DEFAULT NULL,
  `Full_scan_sum` float DEFAULT NULL,
  `Full_join_cnt` float DEFAULT NULL,
  `Full_join_sum` float DEFAULT NULL,
  `Tmp_table_cnt` float DEFAULT NULL,
  `Tmp_table_sum` float DEFAULT NULL,
  `Tmp_table_on_disk_cnt` float DEFAULT NULL,
  `Tmp_table_on_disk_sum` float DEFAULT NULL,
  `Filesort_cnt` float DEFAULT NULL,
  `Filesort_sum` float DEFAULT NULL,
  `Filesort_on_disk_cnt` float DEFAULT NULL,
  `Filesort_on_disk_sum` float DEFAULT NULL,
  PRIMARY KEY (`checksum`,`ts_min`,`ts_max`),
  KEY `idx_serverid_max` (`serverid_max`) USING BTREE,
  KEY `idx_checksum` (`checksum`) USING BTREE,
  KEY `idx_query_time_max` (`Query_time_max`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mysql_slow_query_review_history`
--

LOCK TABLES `mysql_slow_query_review_history` WRITE;
/*!40000 ALTER TABLE `mysql_slow_query_review_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_slow_query_review_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_product`
--

DROP TABLE IF EXISTS `products_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `name_cn` varchar(10) NOT NULL,
  `op_interface` varchar(150) NOT NULL,
  `dev_interface` varchar(150) NOT NULL,
  `level` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_product_name_cn_f625cd1f` (`name_cn`),
  KEY `products_product_pid_02fb4fe7` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_product`
--

LOCK TABLES `products_product` WRITE;
/*!40000 ALTER TABLE `products_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources_newserver`
--

DROP TABLE IF EXISTS `resources_newserver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_newserver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(64) NOT NULL,
  `ip_inner` varchar(32) NOT NULL,
  `port` int(11) NOT NULL,
  `os_status` int(11) NOT NULL,
  `system_status` int(11) NOT NULL,
  `cpu_info` varchar(64) NOT NULL,
  `cpu_count` int(11) NOT NULL,
  `mem_info` varchar(32) NOT NULL,
  `os_system` varchar(32) NOT NULL,
  `os_system_num` int(11) NOT NULL,
  `uuid` varchar(64) NOT NULL,
  `sn` varchar(64) NOT NULL,
  `scan_status` int(11) NOT NULL,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `server_user_id` int(11) DEFAULT NULL,
  `server_group_id` int(11) DEFAULT NULL,
  `chart_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_inner` (`ip_inner`),
  KEY `resources_newserver_server_user_id_f6217936_fk_resources` (`server_user_id`),
  KEY `resources_newserver_server_group_id_013eb012_fk_resources` (`server_group_id`),
  CONSTRAINT `resources_newserver_server_group_id_013eb012_fk_resources` FOREIGN KEY (`server_group_id`) REFERENCES `resources_servergroup` (`id`),
  CONSTRAINT `resources_newserver_server_user_id_f6217936_fk_resources` FOREIGN KEY (`server_user_id`) REFERENCES `resources_serveruser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_newserver`
--

LOCK TABLES `resources_newserver` WRITE;
/*!40000 ALTER TABLE `resources_newserver` DISABLE KEYS */;
/*!40000 ALTER TABLE `resources_newserver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources_servergroup`
--

DROP TABLE IF EXISTS `resources_servergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_servergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `utime` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_servergroup`
--

LOCK TABLES `resources_servergroup` WRITE;
/*!40000 ALTER TABLE `resources_servergroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `resources_servergroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources_serveruser`
--

DROP TABLE IF EXISTS `resources_serveruser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_serveruser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `info` longtext NOT NULL,
  `privatekey` longtext NOT NULL,
  `pubkey` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_serveruser`
--

LOCK TABLES `resources_serveruser` WRITE;
/*!40000 ALTER TABLE `resources_serveruser` DISABLE KEYS */;
/*!40000 ALTER TABLE `resources_serveruser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groupprofile`
--

DROP TABLE IF EXISTS `users_groupprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groupprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` longtext NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`),
  CONSTRAINT `users_groupprofile_group_id_86f9a474_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groupprofile`
--

LOCK TABLES `users_groupprofile` WRITE;
/*!40000 ALTER TABLE `users_groupprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_groupprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `weixin` varchar(32) NOT NULL,
  `info` longtext NOT NULL,
  `lnvalid_date` varchar(32) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `users_profile_user_id_2112e78d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_profile`
--

LOCK TABLES `users_profile` WRITE;
/*!40000 ALTER TABLE `users_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_registeremail`
--

DROP TABLE IF EXISTS `users_registeremail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_registeremail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_code` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `send_time` datetime(6) NOT NULL,
  `active_status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_registeremail_user_id_c1af7f33_fk_auth_user_id` (`user_id`),
  CONSTRAINT `users_registeremail_user_id_c1af7f33_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_registeremail`
--

LOCK TABLES `users_registeremail` WRITE;
/*!40000 ALTER TABLE `users_registeremail` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_registeremail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-23 11:07:31
